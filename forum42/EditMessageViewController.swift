//
//  EditMessageViewController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class EditMessageViewController: UIViewController {

    @IBOutlet weak var addEdit: UIButton!
    @IBOutlet weak var textMessage: UITextView!
    var apiController: APIController?
    var message: Message?
    
    
    @IBAction func click(_ sender: Any) {
        let trimmed = textMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmed == "") {
            return
        }
        if (self.title == "Add Message") {
            apiController?.sendMessageTest(topicId: (message?.topic_id)!, messageToSend: trimmed)
            _ = navigationController?.popViewController(animated: true)
        }
        else if (self.title == "Edit Message"){
            message?.content = trimmed
            apiController?.editMessage(message: message!)
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textMessage.delegate = self as UITextViewDelegate
        self.apiController = APIController(viewController: self)
    }
    
    func setMessage(message: Message) {
        self.message = message
    }

}

extension EditMessageViewController: UITextFieldDelegate, UITextViewDelegate {
    
}
