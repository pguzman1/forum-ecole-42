//
//  MessageTableViewCellDelegate.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import Foundation

protocol MessageTableViewCellDelegate {
    
    func DeleteThisMessage(message: Message, indexPath: IndexPath)
    func SetSelectedMessage(message: Message)
    func Showreplies(message: Message)
    
    
}
