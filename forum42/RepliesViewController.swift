//
//  RepliesViewController.swift
//  forum42
//
//  Created by Jonas BELLESSA on 10/8/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class RepliesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

 
    
    var apiController : APIController?
    var topic_id : Int = 4816
    var firstMessage : Message?
    var myId: Int?
    @IBOutlet weak var tableView: UITableView!
    
    var replies: [Message]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableViewAutomaticDimension
        apiController = APIController(viewController: self)

        apiController?.getReplies(message: firstMessage!, from: 0, to: 30)
    }
    
    func receive(messages: [Message]) {
        print("receive Messages")
        self.replies = messages
        let defaults: UserDefaults? = UserDefaults.standard
        myId = defaults?.object(forKey: "myId") as? Int
        print(messages.count)
        self.tableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return replies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repliesCell") as? RepliesTableviewCell
        let mess = self.replies[indexPath.row]
        cell?.setMessage(message: mess, Myid: myId!)
        return cell!
    }
    
}
