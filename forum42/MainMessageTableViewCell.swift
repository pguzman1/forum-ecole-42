//
//  MainMessageTableViewCell.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class MainMessageTableViewCell: UITableViewCell {

    var delegate : MessageTableViewCellDelegate?
    var message : Message?
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    func setMessage(message: Message, Myid : Int){
        self.message = message
        contentLabel.text = message.content
        nameLabel.text = message.author["name"] as? String
        if (message.updated_at?.characters.count)! > 11 {
            let tempText = String(message.updated_at!.characters.dropLast(8))
            let dateFormatted = tempText.replacingOccurrences(of: "T", with: " ", options: .literal, range: nil)
            print(dateFormatted)
            dateLabel.text = dateFormatted
        }
        else {
            dateLabel.text = message.updated_at
        }
        
    
    }
    @IBAction func repliesButton(_ sender: UIButton) {
        if let del = self.delegate{
            if self.message != nil{
                del.Showreplies(message: self.message!)
            }
        }
    }

}
