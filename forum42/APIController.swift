//
//  APIController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import Foundation
import UIKit

class APIController {
    var delegate: APIDelegate?
    var mainView: MainViewController?
    var topicsView: TopicsViewController?
    var messagesView:  MessagesViewController?
    var repliesView:  RepliesViewController?
    var editView: EditTopicViewController?

    var user: User?
    var token: String?
 
    func connection() {
        let redirect_uri = "swt://inner".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        UIApplication.shared.open(NSURL(string: "https://api.intra.42.fr/oauth/authorize?client_id="+client_id+"&redirect_uri=" + redirect_uri! + "&response_type=code&scope=public%20forum&state=coucou")! as URL, options: [:], completionHandler: nil)
    }
    
    
    // where do we go if we click over DONT ALLOW IN 42 ? 
    
    func getToken(code: String){
        print("getToken\n")
        
        let redirect_uri = "swt://inner".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        let myurl = NSURL(string: "https://api.intra.42.fr/oauth/token")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)

        myrequest.httpMethod = "POST"
        myrequest.httpBody = "grant_type=authorization_code&client_id=\(client_id)&client_secret=\(client_secret)&code=\(code)&redirect_uri=\(redirect_uri!)&state=coucou".data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print(err)
            }
            else if let d = data {
                do {
                    if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                        if (dic["access_token"] as? String) != nil {
                            let token = dic["access_token"] as? String
                            UserDefaults.standard.set(token, forKey: "token")
                            
                            if let top = self.topicsView{
                                print("PASSING TOKEN")
                                top.token = token
                            }
                            print("saving token in userdefaults")
                            self.getUser(token: token!)
                        }
                    }
                }
                catch(let error) {
                    print(error)
                }
            }
            
        }
        task.resume()
    }

    func getUser(token: String){
        print("GET USER\n\n")
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/me")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "GET"
        if (self.token == nil) {
            self.token = token
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                do {
                    if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                        print ("I ame\n\n")
                        if let id = dic["id"] as? Int {
                            UserDefaults.standard.set(id, forKey: "myId")
                            if let topicsView = self.topicsView {
                                topicsView.myId = id
                            }
                        }
                    }else{
                        print("failed parse")
                    }
                }
                catch(let error) {
                    print ("error2 : ")
                    print(error)
                }
            }
            
        }
        task.resume()
    }
    
    func disconectUser(user: User) {
        
    }
    
    // COULD NOT USE PARAM 'PAGE', I GUESS WE WILL JUST SHOW 30 FIRT TOPICS
    
    func getTopics(token: String) {
        var topicsToPass: [Topic] = []
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "GET"
        if (self.token == nil) {
            self.token = token
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                do {
                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: Any]] {
                        for elem in dic {
                            let titleTopic = elem["name"]!
                            let created_at = elem["created_at"]
                            let id = elem["id"]
                            var authorName: String?
                            var authorId: Int?
                            var details: String?
                            var authorTopic: [String: Any]?
                            
                            if let author = elem["author"] as? [String: Any] {
                                if let id = author["id"] as? Int {
                                    authorId = id
                                }
                                if let login = author["login"] as? String {
                                    authorName = login
                                }
                                authorTopic = ["id": authorId ?? 123, "name": authorName ?? "no name"]  /// DEFAULT VALUES, MAYBE NOT A GOOD IDEA ??
                             }
                            if let message = elem["message"] as? [String: Any] {
                                if let content = message["content"] as? [String: String] {
                                    details = content["markdown"]
                                }
                            }
                            topicsToPass.append(Topic(title: titleTopic as? String, author: authorTopic!, details: details, created_at: created_at as? String, id: id as? Int))
                        }
                        DispatchQueue.main.async {
                            self.topicsView?.receive(topics: topicsToPass)
                        }
                    }else{
                        print("failed parse. Retry...")
                        self.getTopics(token: token)
                    }
                }
                catch(let error) {
                    print ("error2 : ")
                    print(error)
                }
            }
            
        }
        task.resume()
    }
    
    func getMessages(topic: Topic, from: Int, to: Int) {
        var messagessToPass: [Message] = []
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics/\(topic.id!)/messages")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "GET"
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                do {
                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [NSDictionary] {
                        for elem in dic {
                            let content = elem["content"]
                            let updated_at = elem["updated_at"]
                            let id = elem["id"]
                            var authorName: String?
                            var authorId: Int?
                            var authorMessage: [String: Any]?
                            if let author = elem["author"] as? [String: Any] {
                                if let id = author["id"] as? Int {
                                    authorId = id
                                }
                                if let login = author["login"] as? String {
                                    authorName = login
                                }
                                authorMessage = ["id": authorId ?? 123, "name": authorName ?? "no name"]  /// DEFAULT VALUES, MAYBE NOT A GOOD IDEA ??
                            }
                            messagessToPass.append(Message(id: id as? Int,topic_id : topic.id! , hasReplies: false, content: content as? String, author: authorMessage!, updated_at: updated_at as? String))
                        }
                        DispatchQueue.main.async {
                            self.messagesView?.receive(messages: messagessToPass)
                        }
                    }else{
                        print("failed parse. Retry...")
                        self.getMessages(topic: topic, from: from, to: to)
                    }
                }
                catch(let error) {
                    print ("error2 : ")
                    print(error)
                }
            }
            
        }
        task.resume()
    }
    
    func getReplies(message: Message, from: Int, to: Int) {
        var messagessToPass: [Message] = []
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/messages/\(message.id!)/messages")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "GET"
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                do {
                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String:Any]] {
                        for elem in dic {
                            let content = elem["content"]
                            let updated_at = elem["created_at"]
                            let id = elem["id"]
                            var authorName: String?
                            var authorId: Int?
                            var authorMessage: [String: Any]?
                            if let author = elem["author"] as? [String: Any] {
                                if let id = author["id"] as? Int {
                                    authorId = id
                                }
                                if let login = author["login"] as? String {
                                    authorName = login
                                }
                                authorMessage = ["id": authorId ?? 123, "name": authorName ?? "no name"]  /// DEFAULT VALUES, MAYBE NOT A GOOD IDEA ??
                            }
                            messagessToPass.append(Message(id: id as? Int,topic_id : nil, hasReplies: false, content: content as? String, author: authorMessage!, updated_at: updated_at as? String))
                          }
                        DispatchQueue.main.async {
                            self.repliesView?.receive(messages: messagessToPass)
                        }
                    }else{
                        print("failed parse. Retry...")
                        self.getReplies(message: message, from: from, to: to)
                    }
                }
                catch(let error) {
                    print ("error2 : ")
                    print(error)
                }
            }
            
        }
        task.resume()
    }
    
    func createMessage(topic: Topic, message: Message) {
        
    }
    
    
    func deleteMessage(message: Message) {
        
    }
    
    func createTopic(topic: Topic) {
//        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics")
//        let myrequest = NSMutableURLRequest(url: myurl! as URL)
//        myrequest.httpMethod = "POST"
//        if (self.token == nil) {
//            self.token = UserDefaults.standard.string(forKey: "token")!
//        }
//        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
//        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
//            (data, response, error) in
//            if let err = error {
//                print ("error : ")
//                print(err)
//            }
//            else if let d = data {
//                do {
//                    print(response)
//                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String:Any]] {
//                             print (dic)
//                        for elem in dic {
//                            
//                        }
//                        DispatchQueue.main.async {
////                            self.messagesView?.receive(messages: messagessToPass)
//                        }
//                    }else{
//                        print("failed parse")
//                    }
//                }
//                catch(let error) {
//                    print ("error2 : ")
//                    print(error)
//                }
//            }
//            
//        }
//        task.resume()
    }
    
    func editTopic(topic: Topic) {
        
    }
    
    func deleteTopic(topic: Topic) {
        
    }
    
    func sendMessageTest(topicId: Int, messageToSend: String) {
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics/\(topicId)/messages")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        let myId: Int?
        myId = UserDefaults.standard.object(forKey: "myId")! as? Int
        let json: [String: Any] = ["message": ["content": messageToSend], "author_id": myId!]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        print(jsonData!)
        myrequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        myrequest.httpMethod = "POST"
        myrequest.httpBody = jsonData
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response)
            }
            
        }
        task.resume()
    }
    
    func deleteMessage(messageId : Int) {
        let messageIdToDelete = messageId
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/messages/\(messageIdToDelete)")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "DELETE"
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response)
            }
            
        }
        task.resume()
    }
    
    func editMessage(message: Message) {
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/messages/\(message.id!)")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        let myId: Int?
        myId = UserDefaults.standard.object(forKey: "myId")! as? Int
        let json: [String: Any] = ["message": ["content": message.content], "author_id": myId!]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        myrequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        myrequest.httpMethod = "PUT"
        myrequest.httpBody = jsonData
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response)
            }
            
        }
        task.resume()
    }
    
    func editTopicTest(topic: Topic) {
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics/\(topic.id!)")
        print(myurl)
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        let json: [String: Any] = ["topic": ["name": topic.title!]]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        myrequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        myrequest.httpMethod = "PUT"
        myrequest.httpBody = jsonData
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response)
            }
            
        }
        task.resume()
        
        
    }
    func deleteTopicTest(topicId: Int) {
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics/\(topicId)")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "DELETE"
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response)
            }
        }
        task.resume()
    }
    
    
    func createTopicTest(topic: Topic) {
        let myurl = NSURL(string: "https://api.intra.42.fr/v2/topics")
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        let myId: Int?
        myId = UserDefaults.standard.object(forKey: "myId")! as? Int
        let json: [String: Any] = ["topic":[ "kind": "normal", "cursus_ids": ["1"],"language_id": "1","messages_attributes":[["author_id": myId!,"content": topic.details!]],"name": topic.title!,"tag_ids": ["8"]]]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        myrequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        myrequest.httpMethod = "POST"
        myrequest.httpBody = jsonData
        if (self.token == nil) {
            self.token = UserDefaults.standard.string(forKey: "token")!
        }
        myrequest.setValue("Bearer \(self.token!)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print ("error : ")
                print(err)
            }
            else if let d = data {
                print(response!)
                do {
                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [Any] {
                        print(dic)
                    }
                }
                catch(let err) {
                    print(err)
                }
            }
            
        }
        task.resume()
        
        
    }
    
    init(viewController: UIViewController) {
        token = UserDefaults.standard.object(forKey: "token") as? String
        print(token)
        if viewController is MainViewController {
            self.mainView = viewController as? MainViewController
        }
        else if viewController is TopicsViewController {
            self.topicsView = viewController as? TopicsViewController
        }
        else if viewController is MessagesViewController{
            self.messagesView = viewController as? MessagesViewController
        }
        else if viewController is EditTopicViewController {
            self.editView = viewController as? EditTopicViewController
        }
        else if viewController is RepliesViewController{
            self.repliesView = viewController as? RepliesViewController
        }
    }
    
}
