//
//  MessageTableViewCell.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    var delegate : MessageTableViewCellDelegate?

    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBAction func editButton(_ sender: UIButton) {
        if let del = self.delegate{
            if self.message != nil{
                del.SetSelectedMessage(message: self.message!)
            }
        }
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        if let del = self.delegate{
            if self.message != nil{
                del.DeleteThisMessage(message: self.message!, indexPath: self.indexPath!)
            }
        }
    }
    var message : Message?
    var indexPath: IndexPath?
    var isMain : Bool = false{
        didSet{
            if isMain{
                self.deleteButton.isHidden = true
            }
        }
    }
    
    func setMessage(message: Message, Myid : Int){
        self.isMain = false
        deleteButton.isHidden = false
        editButton.isHidden = false
        self.message = message
        contentLabel.text = message.content
        nameLabel.text = message.author["name"] as? String
        if (message.updated_at?.characters.count)! > 11 {
            let tempText = String(message.updated_at!.characters.dropLast(8))
            let dateFormatted = tempText.replacingOccurrences(of: "T", with: " ", options: .literal, range: nil)
            print(dateFormatted)
            dateLabel.text = dateFormatted
        }
        else {
            dateLabel.text = message.updated_at
        }
        if let uid = message.author["id"] as? Int{
            if uid != Myid{
                deleteButton.isHidden = true
                editButton.isHidden = true
            }
        }
    }
    
    @IBAction func repliesButton(_ sender: UIButton) {
        if let del = self.delegate{
            if self.message != nil{
                del.Showreplies(message: self.message!)
            }
        }
    }
    
}
