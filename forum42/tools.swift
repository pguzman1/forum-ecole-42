//
//  tools.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import Foundation


struct Message {
    var id: Int?
    var topic_id: Int?
    var hasReplies: Bool?
    var content: String?
    var author: [String:Any]
    var updated_at : String?
}

struct User {
    
}

struct Topic {
    var title: String?
    var author: [String: Any]
    var details: String?
    var created_at: String?
    var id: Int?
}

