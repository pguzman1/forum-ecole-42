//
//  TopicsViewController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class TopicsViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    var topics: [Topic] = []
    var apiController: APIController?
    var alreadySETBOTH: Bool = false
    var refresh: UIControl?
    var myId: Int? {
        didSet{
            
        }
        willSet(newValue) {
            if newValue != nil {
                if token != nil, alreadySETBOTH == false {
                    alreadySETBOTH = true
                    apiController?.getTopics(token: self.token!)
                }
            }
        }
    }
    var selectedTopic: Topic?
    
    var token : String?{
        didSet{
            
        }
        willSet(newValue) {
            if newValue != nil {
                if myId != nil, alreadySETBOTH == false  {
                    alreadySETBOTH = true
                    apiController?.getTopics(token: (newValue)!)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // RELOAD JUST WHEN THE USER COME FROM ANOTHER VIEW BUT THE MAIN ONE
        if (self.token != nil) {
            apiController?.getTopics(token: self.token!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        table.delegate = self
        table.estimatedRowHeight = 300
        table.rowHeight = UITableViewAutomaticDimension
        apiController = APIController(viewController: self)
        apiController?.getToken(code: UserDefaults.standard.string(forKey: "code")!)
        if let tok = UserDefaults.standard.string(forKey: "token") {
            token = tok
        }
        if let id = UserDefaults.standard.object(forKey: "myId") {
            myId = id as? Int
        }
        if token != nil, myId != nil {
            apiController?.getTopics(token: token!)
        }
        refresh = UIRefreshControl()
        table.addSubview(refresh!)
        table.refreshControl = refresh as! UIRefreshControl
        table.refreshControl?.addTarget(self, action: #selector(refreshTable), for: UIControlEvents.valueChanged)
        
    }
    
    func refreshTable() {
        print("REFRESHING!!")
        apiController?.getTopics(token: self.token!)
         self.table.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.table.refreshControl?.endRefreshing()
        }
    }

    func receive(topics: [Topic]) {
        print("receive Topics")
        self.topics = topics
        let defaults: UserDefaults? = UserDefaults.standard
        if let id = defaults?.object(forKey: "myId") {
            self.myId = id as! Int
        }
        print(topics.count)
        self.table.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TopicToAdd" {
            let editTopicViewController: EditTopicViewController = segue.destination as! EditTopicViewController
            editTopicViewController.title = "Add Topic"
        }
        else if segue.identifier == "TopicsToEditTopic" {
            let editTopicViewController: EditTopicViewController = segue.destination as! EditTopicViewController
            editTopicViewController.setTopic(topic: selectedTopic!)
            editTopicViewController.title = "Edit Topic"
        }
        else if segue.identifier == "TopicsToMain" {
            let mainViewController: MainViewController = segue.destination as! MainViewController
            // do something
        }
        else if segue.identifier == "TopicsToMessages"{
            if let dest = segue.destination as? MessagesViewController{
                dest.topic = self.selectedTopic
            }
        }
    }
    
    @IBAction func addTopic(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "TopicToAdd", sender: nil)
    }
    @IBAction func disconnect(_ sender: UIBarButtonItem) {
        //delete all the things 
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "myId")
        print("DISCONNECT")
        performSegue(withIdentifier: "TopicsToMain", sender: nil)
    }
}

extension TopicsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "TopicTableViewCell") as! TopicTableViewCell
        let topic = topics[indexPath.row]
        cell.delegate = self
        cell.setCell(topic: topic, myId: myId!, indexPath: indexPath) // I GOT MYID NIL (ONE TIME)!!!!!! :(
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topics.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(topics[indexPath.row])
        self.selectedTopic = topics[indexPath.row]
        if apiController != nil {
            print("APICONTR IS GOING TO SEND A MESSAGE")
        }
//        apiController?.sendMessageTest(topicId: topics[indexPath.row].id!)                            // TEST
//        apiController?.deleteMessageTest()
//        apiController?.editMessageTest()
//        apiController?.editTopicTest()
//        apiController?.deleteTopicTest()
//        apiController?.createTopicTest()
        
        performSegue(withIdentifier: "TopicsToMessages", sender: nil)
    }
    
}

extension TopicsViewController: TopicTableViewCellDelegate {
    
    func setSelectedTopic(topic: Topic) {
        self.selectedTopic = topic
        performSegue(withIdentifier: "TopicsToEditTopic", sender: nil)
        print("topic was set selected")
    }
    
    func deleteThisTopic(topic: Topic, indexPath: IndexPath) {
        let errorMessage = "Are you sure you want to delete it?"
        let alert = UIAlertController(title: "Delete Topic", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.apiController?.deleteTopicTest(topicId: topic.id!)
            self.topics.remove(at: indexPath.row)
            self.table.deleteRows(at: [indexPath], with: .automatic)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
