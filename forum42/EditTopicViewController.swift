//
//  EditTopicViewController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class EditTopicViewController: UIViewController {

    @IBOutlet weak var topicTitle: UITextField!
    @IBOutlet weak var topicText: UITextView!
    @IBOutlet weak var edit: UIButton!
    var titleToSet: String?
    var detailsToSet: String?
    var apiController: APIController?
    var topic_id: Int?
    var author: [String:Any]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topicText.delegate = self as UITextViewDelegate
        topicTitle.delegate = self as UITextFieldDelegate
        topicTitle.text = titleToSet
        topicText.text = detailsToSet
        apiController = APIController(viewController: self)
        if self.title == "Edit Topic" {
            topicText.isEditable = false
        }
    }
    
    func setTopic(topic: Topic) {
        titleToSet = topic.title!
        detailsToSet = topic.details!
        self.topic_id = topic.id
        self.author = topic.author
    }

    @IBAction func clickEditAdd(_ sender: Any) {
        let trimmedTitle = topicTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let trimmedText = topicText.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedText == nil, trimmedTitle == nil {
            return
        }
        if (self.title == "Add Topic") {
            apiController?.createTopicTest(topic: Topic(title: trimmedTitle, author: [:], details: trimmedText, created_at: nil, id: self.topic_id))
             _ = navigationController?.popViewController(animated: true)
        }
        else if (self.title == "Edit Topic") {
            apiController?.editTopicTest(topic: Topic(title: trimmedTitle, author: [:], details: trimmedText, created_at: nil, id: self.topic_id))
            _ = navigationController?.popViewController(animated: true)
        }
    }

}

extension EditTopicViewController: UITextFieldDelegate, UITextViewDelegate {
    
}
