//
//  ViewController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var logoApp: UIImageView!
    @IBOutlet weak var connect: UIButton!

    var apiController : APIController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiController = APIController(viewController: self)
        navigationController?.isNavigationBarHidden = true
//        if let token = UserDefaults.standard.string(forKey: "token") {
//            if let myId = UserDefaults.standard.string(forKey: "myId") {
//                if myId != nil && token != nil {
//                    print("THE TOKEN AND MYID ARE HEEERE")
//                    performSegue(withIdentifier: "MainToTopics", sender: nil)
//                }
//            }
//        }
    }
    
    @IBAction func clickConnect(_ sender: UIButton) {
        apiController?.connection()
    }
    
    func sendCodeToApiController(code: String) {
        apiController = APIController(viewController: self)
        apiController?.getToken(code: code)
    }
}
