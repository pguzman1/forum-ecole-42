//
//  MessagesViewController.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, MessageTableViewCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var apiController : APIController?
    var topic : Topic?
    var myId: Int?
    var messages: [Message]=[]
    var selectedMessage : Message?
    
    @IBAction func clickAdd(_ sender: Any) {
        selectedMessage = Message(id: nil, topic_id: topic?.id, hasReplies: false, content: nil, author: [:], updated_at: nil)
        performSegue(withIdentifier: "MessagesToAdd", sender: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        apiController?.getMessages(topic: self.topic!, from: 0, to: 20)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableViewAutomaticDimension
        apiController = APIController(viewController: self)
        apiController?.getMessages(topic: topic!, from: 0, to: 30)
    }
    
    func receive(messages: [Message]) {
        print("receive Messages")
        self.messages = messages
        let defaults: UserDefaults? = UserDefaults.standard
        myId = defaults?.object(forKey: "myId") as? Int
        print(messages.count)
       self.tableView.reloadData()
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MessagesToReplies"{
            if let dest = segue.destination as? RepliesViewController{
                dest.firstMessage = self.selectedMessage
            }
        }
        else if segue.identifier == "MessagesToAdd" {
            if let dest = segue.destination as? EditMessageViewController {
                dest.title = "Add Message"
                dest.setMessage(message: selectedMessage!)
            }
        }
        else if segue.identifier == "MessagesToEditMessage" {
            if let editMessageViewController: EditMessageViewController = segue.destination as? EditMessageViewController{
                editMessageViewController.setMessage(message: selectedMessage!)
                editMessageViewController.title = "Edit Message"
            }
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "messageTableViewCell") as? MessageTableViewCell
        let mess = self.messages[indexPath.row]
        cell?.delegate = self
        cell?.indexPath = indexPath
        cell?.setMessage(message: mess, Myid: myId!)
        if (indexPath.row == 0){
         cell?.isMain = true
        }
        return cell!
    }
    
        
    func DeleteThisMessage(message: Message, indexPath: IndexPath){
        let errorMessage = "Are you sure you want to delete it?"
        let alert = UIAlertController(title: "Delete Topic", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            print ("arrive ici0")
            self.apiController?.deleteMessage(messageId: message.id!)
            self.messages.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            print ("arrive ici1")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func SetSelectedMessage(message: Message){
        self.selectedMessage = message
        performSegue(withIdentifier: "MessagesToEditMessage", sender: nil)
        print("topic was set selected")
    }
    func Showreplies(message: Message){
        self.selectedMessage = message
        performSegue(withIdentifier: "MessagesToReplies", sender: "")
    }
    
}


