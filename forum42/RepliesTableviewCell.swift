//
//  RepliesTableviewCell.swift
//  forum42
//
//  Created by Jonas BELLESSA on 10/8/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class RepliesTableviewCell: UITableViewCell {

    var delegate : MessageTableViewCellDelegate?
    var message : Message?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    
    func setMessage(message: Message, Myid : Int){
        self.message = message
        contentLabel.text = message.content
        nameLabel.text = message.author["name"] as? String
        if let da = message.updated_at{
        if (da.characters.count) > 11 {
            let tempText = String(message.updated_at!.characters.dropLast(8))
            let dateFormatted = tempText.replacingOccurrences(of: "T", with: " ", options: .literal, range: nil)
            print(dateFormatted)
            dateLabel.text = dateFormatted
        }
        else {
            dateLabel.text = message.updated_at
        }
        }
        
        
    }

}
