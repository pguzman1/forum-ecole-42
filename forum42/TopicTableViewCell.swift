//
//  TopicTableViewCell.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var authorName: UILabel!
    var authorId: Int?
    var delegate: TopicTableViewCellDelegate?
    var author: [String: Any]?
    var topicText: String?
    var topic_id: Int?
    var topic : Topic?
    var indexPath: IndexPath?

    func setCell(topic: Topic, myId: Int, indexPath: IndexPath) {
        edit.isHidden = false
        delete.isHidden = false
        topic_id = topic.id!
        
        self.indexPath = indexPath
        title.text = topic.title
        topicText = topic.details
        if (topic.created_at?.characters.count)! > 11 {
            let tempText = String(topic.created_at!.characters.dropLast(8))
            let dateFormatted = tempText.replacingOccurrences(of: "T", with: " ", options: .literal, range: nil)
            print(dateFormatted)
            date.text = dateFormatted
        }
        else {
            date.text = topic.created_at
        }
        authorId = topic.author["id"] as? Int
        author = topic.author
        if (authorId != myId) {
            edit.isHidden = true
            delete.isHidden = true
        }
        authorName.text = topic.author["name"] as? String
        self.topic = topic
    }
    @IBAction func clickEdit(_ sender: UIButton) {
        print("EDIT WAS CLICKED")
        let topicSelected = self.topic
        print(topicSelected)
        delegate?.setSelectedTopic(topic: topicSelected!)
    }

    @IBAction func clickDelete(_ sender: Any) {
        let topicToDelete = self.topic
        delegate?.deleteThisTopic(topic: topicToDelete!, indexPath: self.indexPath!)
    }
    
    
}
