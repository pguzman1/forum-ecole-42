//
//  TopicTableViewCellDelegate.swift
//  forum42
//
//  Created by Patricio GUZMAN on 10/7/17.
//  Copyright © 2017 team42. All rights reserved.
//

import Foundation

protocol TopicTableViewCellDelegate {
    
    func setSelectedTopic(topic: Topic)
    func deleteThisTopic(topic: Topic, indexPath: IndexPath)
}
